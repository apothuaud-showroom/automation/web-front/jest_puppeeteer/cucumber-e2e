let puppeteer = require('puppeteer');
let fs = require('fs');
let PNG = require('pngjs').PNG;
let pixelmatch = require('pixelmatch');

describe('Google Desktop search page', () => {

    let browser;
    let page;

    beforeEach(async () => {
        browser = await puppeteer.launch();
        page = await browser.newPage();
        await page.setViewport({
            width: 1280,
            height: 720,
            deviceScaleFactor: 1,
        });  
        await page.goto("https://www.google.com/search?hl=en&q=puppeteer+wendigo&oq=puppeteer+wendigo");
    });

    test("searchform conserves UI", async () => {
        const searchform = await page.$('#searchform');
        await searchform.screenshot({
            path: './screenshots/google.searchform.desktop.actual.png'
        });
        const img1 = PNG.sync.read(fs.readFileSync('./screenshots/google.searchform.desktop.ref.png'));
        const img2 = PNG.sync.read(fs.readFileSync('./screenshots/google.searchform.desktop.actual.png'));
        const { width, height } = img2;
        const diff = new PNG({ width, height });
        const numDiffPixels = pixelmatch(img1.data, img2.data, diff.data, width, height, {threshold: 0.1});
        fs.writeFileSync('./screenshots/google.searchform.desktop.diff.png', PNG.sync.write(diff));
        expect(numDiffPixels).toBe(0);
    }, 30000);

    test("top_nav conserves UI", async () => {
        const top_nav = await page.$('#top_nav');
        await top_nav.screenshot({
            path: './screenshots/google.top_nav.desktop.actual.png'
        });
        const img1 = PNG.sync.read(fs.readFileSync('./screenshots/google.top_nav.desktop.ref.png'));
        const img2 = PNG.sync.read(fs.readFileSync('./screenshots/google.top_nav.desktop.actual.png'));
        const { width, height } = img2;
        const diff = new PNG({ width, height });
        const numDiffPixels = pixelmatch(img1.data, img2.data, diff.data, width, height, {threshold: 0.1});
        fs.writeFileSync('./screenshots/google.top_nav.desktop.diff.png', PNG.sync.write(diff));
        expect(numDiffPixels).toBe(0);
    }, 30000);

    test("res conserves UI", async () => {
        const res = await page.$('.srg div');
        await res.screenshot({
            path: './screenshots/google.res.desktop.actual.png'
        });
        const img1 = PNG.sync.read(fs.readFileSync('./screenshots/google.res.desktop.ref.png'));
        const img2 = PNG.sync.read(fs.readFileSync('./screenshots/google.res.desktop.actual.png'));
        const { width, height } = img2;
        const diff = new PNG({ width, height });
        const numDiffPixels = pixelmatch(img1.data, img2.data, diff.data, width, height, {threshold: 0.1});
        fs.writeFileSync('./screenshots/google.res.desktop.diff.png', PNG.sync.write(diff));
        expect(numDiffPixels).toBe(0);
    }, 30000);

    afterEach(async () => {
        await browser.close();
    });
})