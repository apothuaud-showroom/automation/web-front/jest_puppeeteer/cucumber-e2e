let puppeteer = require('puppeteer');
let fs = require('fs');
let PNG = require('pngjs').PNG;
let pixelmatch = require('pixelmatch');

describe('Google Desktop home page', () => {

    let browser;
    let page;

    beforeEach(async () => {
        browser = await puppeteer.launch();
        page = await browser.newPage();
        await page.setViewport({
            width: 1280,
            height: 720,
            deviceScaleFactor: 1,
        }); 
        await page.goto("https://www.google.com/?hl=en");
        await page.waitForSelector("#hplogo");
        await page.click("#hplogo");
        const linkHandlers = await page.$x("//*[contains(text(), 'Non, merci')]");
        if (linkHandlers.length > 0) {
            await linkHandlers.click();
        }
    });

    test("page conserves UI", async () => {
        await page.screenshot({
            path: './screenshots/google.home.desktop.actual.png'
        });
        const img1 = PNG.sync.read(fs.readFileSync('./screenshots/google.home.desktop.ref.png'));
        const img2 = PNG.sync.read(fs.readFileSync('./screenshots/google.home.desktop.actual.png'));
        const { width, height } = img1;
        const diff = new PNG({ width, height });
        const numDiffPixels = pixelmatch(img1.data, img2.data, diff.data, width, height, {threshold: 0.1});
        fs.writeFileSync('./screenshots/google.home.desktop.diff.png', PNG.sync.write(diff));
        expect(numDiffPixels).toBe(0);
    }, 30000);

    afterEach(async () => {
        await browser.close();
    });
})