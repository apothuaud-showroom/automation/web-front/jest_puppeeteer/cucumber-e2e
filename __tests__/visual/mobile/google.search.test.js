let puppeteer = require('puppeteer');
let iPhone = puppeteer.devices['iPhone 6'];
let fs = require('fs');
let PNG = require('pngjs').PNG;
let pixelmatch = require('pixelmatch');

describe('Google Mobile search page', () => {

    let browser;
    let page;

    beforeEach(async () => {
        browser = await puppeteer.launch();
        page = await browser.newPage();
        await page.emulate(iPhone);
        await page.goto("https://www.google.com/search?hl=en&q=puppeteer+wendigo&oq=puppeteer+wendigo");
    });

    test("page conserves UI", async () => {
        await page.screenshot({
            path: './screenshots/google.search.mobile.actual.png'
        });
        const img1 = PNG.sync.read(fs.readFileSync('./screenshots/google.search.mobile.ref.png'));
        const img2 = PNG.sync.read(fs.readFileSync('./screenshots/google.search.mobile.actual.png'));
        const { width, height } = img2;
        const diff = new PNG({ width, height });
        const numDiffPixels = pixelmatch(img1.data, img2.data, diff.data, width, height, {threshold: 0.1});
        fs.writeFileSync('./screenshots/google.search.mobile.diff.png', PNG.sync.write(diff));
        expect(numDiffPixels).toBe(0);
    }, 30000);

    afterEach(async () => {
        await page.close();
    });
})