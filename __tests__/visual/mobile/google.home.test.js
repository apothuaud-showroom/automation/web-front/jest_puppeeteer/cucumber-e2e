let puppeteer = require('puppeteer');
let iPhone = puppeteer.devices['iPhone 6'];
let fs = require('fs');
let PNG = require('pngjs').PNG;
let pixelmatch = require('pixelmatch');

describe('Google Mobile home page', () => {

    let browser;
    let page;

    beforeEach(async () => {
        browser = await puppeteer.launch();
        page = await browser.newPage();
        await page.emulate(iPhone);
        await page.goto("https://www.google.com/?hl=en");
        await page.waitForSelector("#hplogo");
        await page.click("#hplogo");
    }, 30000);

    test("page conserves UI", async () => {
        const linkHandlers = await page.$x("//*[contains(text(), 'Already agreed')]");
        if (linkHandlers.length > 0) {
            await linkHandlers[0].click();
        }
        await page.screenshot({
            path: './screenshots/google.home.mobile.actual.png'
        });
        const img1 = PNG.sync.read(fs.readFileSync('./screenshots/google.home.mobile.ref.png'));
        const img2 = PNG.sync.read(fs.readFileSync('./screenshots/google.home.mobile.actual.png'));
        const { width, height } = img1;
        const diff = new PNG({ width, height });
        const numDiffPixels = pixelmatch(img1.data, img2.data, diff.data, width, height, {threshold: 0.1});
        fs.writeFileSync('./screenshots/google.home.mobile.diff.png', PNG.sync.write(diff));
        expect(numDiffPixels).toBe(0);
    }, 30000);

    afterEach(async () => {
        await browser.close();
    });
})