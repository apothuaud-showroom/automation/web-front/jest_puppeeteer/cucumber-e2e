let puppeteer = require('puppeteer');
let iPhone = puppeteer.devices['iPhone 6'];

describe('Google Mobile search page', () => {

    let browser;
    let page;

    beforeEach(async () => {
        browser = await puppeteer.launch();
        page = await browser.newPage();
        await page.emulate(iPhone);      
        await page.goto("https://www.google.com/search?hl=en&q=puppeteer+wendigo&oq=puppeteer+wendigo");
    }, 30000);

    test("displays results stats", async () => {
        await page.waitForSelector('#rso', {
            visible: true,
        })
    }, 30000);

    afterEach(async () => {
        await browser.close();
    });
})