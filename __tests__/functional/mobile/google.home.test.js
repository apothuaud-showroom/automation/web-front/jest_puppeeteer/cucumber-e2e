let puppeteer = require('puppeteer');
let iPhone = puppeteer.devices['iPhone 6'];

describe('Google Mobile home page', () => {

    let browser;
    let page;

    beforeEach(async () => {
        browser = await puppeteer.launch();
        page = await browser.newPage();
        await page.emulate(iPhone);  
        await page.goto("https://www.google.com/?hl=en");
    }, 30000);

    test("displays search bar", async () => {
        await page.waitForSelector('input[name=q]', {
            visible: true,
        })
    }, 30000);

    afterEach(async () => {
        await browser.close();
    });
})