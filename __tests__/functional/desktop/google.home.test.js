let puppeteer = require('puppeteer');

describe('Google Desktop home page', () => {

    let browser;
    let page;

    beforeEach(async () => {
        browser = await puppeteer.launch();
        page = await browser.newPage();
        await page.setViewport({
            width: 1280,
            height: 720,
            deviceScaleFactor: 1,
        });    
        await page.goto("https://www.google.com/?hl=en");
    });

    test("displays search bar", async () => {
        await page.waitForSelector('input[name=q]');
    }, 30000);

    afterEach(async () => {
        await browser.close();
    });
})