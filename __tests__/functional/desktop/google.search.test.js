let puppeteer = require('puppeteer');

describe('Google Desktop search page', () => {

    let browser;
    let page;

    beforeEach(async () => {
        browser = await puppeteer.launch();
        page = await browser.newPage();
        await page.setViewport({
            width: 1280,
            height: 720,
            deviceScaleFactor: 1,
        });          
        await page.goto("https://www.google.com/search?hl=en&q=puppeteer+wendigo&oq=puppeteer+wendigo");
    }, 30000);

    test("displays results stats", async () => {
        await page.waitForSelector('#resultStats');
    }, 30000);

    afterEach(async () => {
        await browser.close();
    });
})