# JEST + Puppeteer - Functional and visual tests framework

## Features

- easy write functional web tests on chromium healdess browser


- run tests on desktop browser and choose screen resolution


        let puppeteer = require('puppeteer');

        describe('Google Desktop home page', () => {

            let browser;
            let page;

            beforeEach(async () => {
                browser = await puppeteer.launch();
                page = await browser.newPage();
                await page.setViewport({
                    width: 1280,
                    height: 720,
                    deviceScaleFactor: 1,
                });    
                await page.goto("https://www.google.com/?hl=en");
            });

            test("displays search bar", async () => {
                await page.waitForSelector('input[name=q]');
            }, 30000);

            afterEach(async () => {
                await browser.close();
            });
        })


- run tests on emulated mobile devices


        let puppeteer = require('puppeteer');
        let iPhone = puppeteer.devices['iPhone 6'];

        describe('Google Mobile home page', () => {

            let browser;
            let page;

            beforeEach(async () => {
                browser = await puppeteer.launch();
                page = await browser.newPage();
                await page.emulate(iPhone);  
                await page.goto("https://www.google.com/?hl=en");
            }, 30000);

            test("displays search bar", async () => {
                await page.waitForSelector('input[name=q]', {
                    visible: true,
                })
            }, 30000);

            afterEach(async () => {
                await browser.close();
            });
        })


- use pixelmatch to compare screenshots of page/elements for visual regression testing

        test("page conserves UI", async () => {
            await page.screenshot({
                path: './screenshots/google.home.desktop.actual.png'
            });
            const img1 = PNG.sync.read(fs.readFileSync('./screenshots/google.home.desktop.ref.png'));
            const img2 = PNG.sync.read(fs.readFileSync('./screenshots/google.home.desktop.actual.png'));
            const { width, height } = img1;
            const diff = new PNG({ width, height });
            const numDiffPixels = pixelmatch(img1.data, img2.data, diff.data, width, height, {threshold: 0.1});
            fs.writeFileSync('./screenshots/google.home.desktop.diff.png', PNG.sync.write(diff));
            expect(numDiffPixels).toBe(0);
        }, 30000);

- report test suites using jest-html-reporters

![report_dashboard][report_dashboard_logo]
![report_details][report_details_logo]


## Usage

- Installation

        git clone ...
        cd ...
        npm install

- Test

        npm test



[report_dashboard_logo]: .readme/report_dashboard.png "Logo Title Text 2"
[report_details_logo]: .readme/report_details.png "Logo Title Text 2"
